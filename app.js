const Discord = require('discord.js');
const fs = require('fs');
const botserver = require('./botserver.js');
const logger = require('./logger.js');

// Save this JSON structure to props.json wherever the 'current' launch directory is (aka, where db.json is saved)
let props = {
  clientId: undefined,
  token: undefined,
  prefix: 'beer',
  botPermissions: 93184,
  port: 8080
};

let db = {};

const save = () => {
  logger.info('DB saved.');
  fs.writeFile('db.json', JSON.stringify(db), err => err && logger.error(err));
  setTimeout(save, 60000);
};

fs.exists('props.json', exists => {
  if (!exists) {
    logger.error('No properties file, unable to start');
    return;
  }
  fs.readFile('props.json', (err, content) => {
    if (err) throw err;
    props = JSON.parse(content);

    fs.exists('db.json', exists => {
      if (!exists) {
        fs.writeFile('db.json', '{}');
      }
      fs.readFile('db.json', (err, content) => {
        if (err) throw err;
        db = JSON.parse(content);
        botserver(props, new Discord.Client(), db);
        setTimeout(save, 60000);
      });
    });
  });
});
