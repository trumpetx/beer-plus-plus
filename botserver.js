const http = require('http');
const logger = require('./logger.js');

const PROPS_KEY = '_x_x_x_props';
const NOT_MEMBERS = [PROPS_KEY];
// https://discordapp.com/developers/docs/topics/permissions
const PERM_ADMIN = 0x00000008;
const LAST_HELP_MESSAGE = 'last_help_message';
const ALT_PREFIX = 'alternate.prefix';
const ALT_ITEM = 'alternate.item';
const ALT_ITEM_PL = 'alternate.item.plural';
const ALT_ITEM_NM = 'alternate.item.name';
const ALT_ITEM_NM_PL = 'alternate.item.name.plural';
const BOOL_CONFIGS = {
  '--.admin_only': prefix =>
    '`' +
    prefix +
    'config --.admin_only true`    Only administrators can execute the -- command. `' +
    prefix +
    'config --.admin_only false` to turn off this setting.',
  '++.admin_only': prefix =>
    '`' +
    prefix +
    'config ++.admin_only true`    Only administrators can execute the ++ command. `' +
    prefix +
    'config ++.admin_only false` to turn off this setting.',
  '##.admin_only': prefix =>
    '`' +
    prefix +
    'config ##.admin_only true`    Only administrators can execute the ## command. `' +
    prefix +
    'config ##.admin_only false` to turn off this setting.',
  '%%.admin_only': prefix =>
    '`' +
    prefix +
    'config %%.admin_only true`    Only administrators can execute the %% command. `' +
    prefix +
    'config %%.admin_only false` to turn off this setting.'
};
const TEXT_CONFIGS = {
  [ALT_PREFIX]: prefix =>
    '`' + prefix + 'config alternate.prefix <prefix>`    Use a different bot prefix (specifically useful if you want to send :wine_glass:, etc)',
  [ALT_ITEM]: prefix =>
    '`' + prefix + "config alternate.item :wine_glass:`    Give something other than `:beer:`!   Don't forget to change the other `alternate.` settings!",
  [ALT_ITEM_PL]: prefix =>
    '`' +
    prefix +
    "config alternate.item.plural :wine_glass::wine_glass:`    Give something other than :beer:!   Don't forget to change the other `alternate.` settings!",
  [ALT_ITEM_NM]: prefix =>
    '`' + prefix + "config alternate.item.name wine`    Give something other than `:beer:`!   Don't forget to change the other `alternate.` settings!",
  [ALT_ITEM_NM_PL]: prefix =>
    '`' + prefix + "config alternate.item.name.plural wine`    Give something other than :beer:!  Don't forget to change the other `alternate.` settings!"
};
const HIDDEN_CONFIGS = [LAST_HELP_MESSAGE];

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function chunk(msg, chunkSize, sendFn) {
  let msgLength = 0;
  let idx = 0;
  for (let i = 0; i < msg.length; i++) {
    if (msgLength + msg[i].length > chunkSize) {
      sendFn(msg.slice(idx, i).join('\n'));
      idx = i;
      msgLength = 0;
      await sleep(1000);
    }
    msgLength += msg[i].length;
  }
  sendFn(msg.slice(idx, msg.length).join('\n'));
}

module.exports = (props, client, db) => {
  const members = message => {
    return message.mentions.everyone
      ? message.guild.members.filter(member => message.isMemberMentioned(member.user) && member.user.username != message.author.username && !member.user.bot)
      : message.mentions.members;
  };

  const beerStr = (num, item, itemPlural) => {
    if (num > 1 || num === 0) {
      return num + ' ' + itemPlural;
    }
    return num + ' ' + item;
  };

  const beerPlusPlus = (guildname, username, num = 1) => {
    let guild = db[guildname] || {};
    let ct = Math.max((guild[username] || 0) + num, 0);
    guild[username] = ct;
    db[guildname] = guild;
    logger.info(guildname, username, ct);
    return ct;
  };

  const getProps = guildname => {
    let guild = db[guildname] || {};
    return guild[PROPS_KEY] || {};
  };

  const saveProps = (guildname, props) => {
    let guild = db[guildname] || {};
    guild[PROPS_KEY] = props;
    db[guildname] = guild;
  };

  const getProp = (propname, guildname, defaultvalue) => {
    defaultvalue === undefined && Object.keys(BOOL_CONFIGS).includes(propname) && (defaultvalue = false);
    let props = getProps(guildname);
    return propname in props ? props[propname] : defaultvalue;
  };

  const saveProp = (propname, propvalue, guildname) => {
    let props = getProps(guildname);
    if (!propvalue) {
      delete props[propname];
    } else {
      props[propname] = propvalue;
    }
    saveProps(guildname, props);
    return props;
  };

  const printHelp = (guildname, prefix, name, namePlural) => {
    const admin = ' [ADMINS ONLY]';
    return (
      'How to Beer-Plus-Plus:```' +
      prefix +
      '++ @Someone @SomeoneElse     Give ' +
      name +
      '!' +
      (getProp('++.admin_only', guildname, false) ? admin : '') +
      '\n' +
      prefix +
      '-- @Soemeone @SomeoneElse    Take ' +
      name +
      '!' +
      (getProp('--.admin_only', guildname, false) ? admin : '') +
      '\n' +
      prefix +
      '%%                           Who has how much ' +
      name +
      '?' +
      (getProp('%%.admin_only', guildname, false) ? admin : '') +
      '\n' +
      prefix +
      '##                           Who has how many ' +
      namePlural +
      '?' +
      (getProp('##.admin_only', guildname, false) ? admin : '') +
      '\n' +
      prefix +
      '??                           This message!\n' +
      '```' +
      'Feedback: <https://discordbots.org/bot/405811389748346881>'
    );
  };

  const getConfiguration = guildname => {
    let msg = ['\nThe current server configuration is (missing values are assumed to be blank/false):'];
    const p = getProps(guildname);
    msg = msg.concat(
      Object.keys(p)
        .filter(k => !HIDDEN_CONFIGS.includes(k))
        .map(k => k + ' = ' + p[k])
    );
    return msg;
  };

  const sendOwnerManual = (user, guildname, prefix) => {
    let msg = ["I noticed you're the owner of this server.  There are some additional configurations available only to you:\n"];
    msg.push('`' + prefix + 'config`    Display Beer-Plus-Plus bot configuration');
    msg.push('`' + prefix + 'reset`    Reset Beer-Plus-Plus bot configuration');
    msg = msg.concat(Object.keys(BOOL_CONFIGS).map(k => BOOL_CONFIGS[k](prefix)));
    msg = msg.concat(Object.keys(TEXT_CONFIGS).map(k => TEXT_CONFIGS[k](prefix)));
    msg = msg.concat(getConfiguration(guildname));
    user.send(msg.join('\n')).catch(logger.error);
  };

  const startsWithIgnoreCase = (str, startsWith) => {
    let substr = str.substring(0, startsWith.length);
    return substr.toLowerCase() === startsWith.toLowerCase();
  };

  const deleteMsg = msg => {
    msg.delete(1000).catch(e => {});
  };
  client.on('error', e => logger.error(e));
  client.on('warn', e => {});
  client.on('debug', e => {});
  client.on('ready', () => {
    logger.info(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds:`);
    client.user.setActivity(`on ${client.guilds.size} servers`).catch(logger.error);
    const guilds = [];
    client.guilds.map(guild => guilds.push(guild.name));
    logger.info(guilds);
    Object.keys(db)
      .filter(k => guilds.indexOf(k) === -1)
      .map(k => {
        logger.info('Purging data from guild: ' + k);
        delete db[k];
      });
  });

  client.on('guildCreate', guild => {
    logger.info(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
    client.user.setActivity(`on ${client.guilds.size} servers`).catch(logger.error);
  });

  client.on('guildDelete', guild => {
    logger.info(`I have been removed from: ${guild.name} (id: ${guild.id})`);
    client.user.setActivity(`on ${client.guilds.size} servers`).catch(logger.error);
    delete db[guild.name];
  });

  client.on('message', async message => {
    if (message.author.bot) return;
    const guildname = message.guild.name;
    const prefix = getProp(ALT_PREFIX, guildname, props.prefix);
    if (!startsWithIgnoreCase(message.content, prefix)) return;

    const args = message.content
      .slice(prefix.length)
      .trim()
      .split(/ +/g);
    const command = args.shift().toLowerCase();

    const isOwner = message.channel.guild.ownerID === message.author.id;
    const isAdmin = isOwner || message.member.hasPermission(PERM_ADMIN);
    const item = getProp(ALT_ITEM, guildname, ':beer:');
    const itemPlural = getProp(ALT_ITEM_PL, guildname, ':beers:');
    const name = getProp(ALT_ITEM_NM, guildname, 'beer');
    const namePlural = getProp(ALT_ITEM_NM_PL, guildname, name + 's');
    let msg = [];
    let reply = [];

    const mapToNick = member => {
      const arr = message.guild.members.array();
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].user.username === member) {
          if (arr[i].nickname === null) {
            break;
          }
          return { userid: member, nick: arr[i].nickname };
        }
      }
      return { userid: member, nick: member };
    };

    if (getProp(command + '.admin_only', guildname, false) && !(isAdmin || isOwner)) {
      message.author.send('The ' + command + ' command can only be used by administrators.').catch(logger.error);
      deleteMsg(message);
      return;
    }

    switch (command) {
      case '++':
        if (!message.mentions.members.size && !message.mentions.everyone) {
          msg.push('Share ' + item + ' with @Someone please!');
        } else {
          members(message).forEach(member => {
            if (member.user.bot) {
              msg.push("Bots don't like " + item + ' we prefer ' + (item == ':beer:' ? ':wine_glass:' : ':beer:'));
            } else if (message.author.username === member.user.username) {
              msg.push("Don't be greedy <@!" + message.author.id + '>!  Give ' + item + ' to @Someone.');
            } else {
              msg.push(
                '<@!' + message.author.id + '> has given <@!' + member.user.id + '> a ' + item + '!  [ ' + beerPlusPlus(guildname, member.user.username) + ' ]'
              );
            }
          });
        }
        break;
      case '--':
        if (!message.mentions.members.size && !message.mentions.everyone) {
          reply.push('Take ' + item + " from @Someone or I'll take it from you!");
        } else {
          members(message).forEach(member => {
            if (member.user.bot) {
              reply.push("Don't mess with my " + item + ' buddy!');
              return;
            } else {
              msg.push(
                '<@!' +
                  message.author.id +
                  '> has taken a ' +
                  item +
                  ' from <@!' +
                  member.user.id +
                  '>!  [ ' +
                  beerPlusPlus(guildname, member.user.username, -1) +
                  ' ]'
              );
            }
          });
        }
        break;
      case '%%':
        let pctMembers = db[guildname];
        let pctMemberKeys = Object.keys(pctMembers).filter(m => !NOT_MEMBERS.includes(m));
        let pctTooMany = pctMemberKeys.length > 20 ? 'Top 20 ' : '';
        msg.push(pctTooMany + item + ' stats for ' + guildname + '! ');
        let total = pctMemberKeys.map(k => pctMembers[k]).reduce((a, b) => a + b);
        pctMemberKeys
          .filter(o => pctMembers[o] > 0)
          .map(mapToNick)
          .sort((o1, o2) => pctMembers[o2.userid] - pctMembers[o1.userid])
          .slice(0, 20)
          .forEach(member => msg.push(member.nick + ' has ' + Math.round((pctMembers[member.userid] / total) * 100) + '% of all ' + itemPlural));
        break;
      case '##':
        let ctMembers = db[guildname];
        let ctMemberKeys = Object.keys(ctMembers).filter(m => !NOT_MEMBERS.includes(m));
        let topX = Math.min(ctMemberKeys.length, 20);
        msg.push('Top ' + topX + ' ' + itemPlural + ' collectors for ' + guildname + '!');
        ctMemberKeys
          .filter(o => ctMembers[o] > 0)
          .map(mapToNick)
          .sort((o1, o2) => ctMembers[o2.userid] - ctMembers[o1.userid])
          .slice(0, 20)
          .forEach(member => msg.push(member.nick + ' has ' + beerStr(ctMembers[member.userid], item, itemPlural)));
        break;
      case '??':
        const lastHelpMessages = getProp(LAST_HELP_MESSAGE, guildname, {});
        let lastHelpMsg = lastHelpMessages[message.channel.id];
        if (lastHelpMsg) {
          message.channel
            .fetchMessage(lastHelpMsg)
            .then(m => m.delete(1000))
            .catch(logger.error);
        }
        message.channel
          .send(printHelp(guildname, prefix, name, namePlural))
          .then(m => {
            lastHelpMessages[message.channel.id] = m.id;
            saveProp(LAST_HELP_MESSAGE, lastHelpMessages, guildname);
          })
          .catch(logger.error);
        if (isOwner) {
          sendOwnerManual(message.author, guildname, prefix);
        }
        deleteMsg(message);
        return;
      case 'reset':
        if (isOwner) {
          saveProps(guildname, {});
          reply.push('Configuration reset!');
        } else {
          reply.push('Only server owners can reset the Beer-Plus-Plus bot configuration');
        }

        break;
      case 'config':
        if (!isOwner) {
          reply.push('Only server owners can update the Beer-Plus-Plus bot configuration');
          break;
        }
        if (args.length == 0) {
          reply = reply.concat(getConfiguration(guildname));
          break;
        }
        const propName = args[0].trim();
        if (args.length == 1) {
          reply.push('Removed/Reset configuration value for: ' + propName);
          saveProp(propName);
          reply = reply.concat(getConfiguration(guildname));
          break;
        }
        const propVal = args[1].trim();
        const isBool = Object.keys(BOOL_CONFIGS).includes(propName);
        const isText = !isBool && Object.keys(TEXT_CONFIGS).includes(propName);
        const isHidden = !isBool && !isText && Object.keys(HIDDEN_CONFIGS).includes(propName);
        if (isBool || isText || isHidden) {
          saveProp(propName, isBool ? propVal === 'true' : propVal, guildname);
          reply.push('Configuration updated!');
          reply = reply.concat(getConfiguration(guildname));
        }
        break;
      default:
        reply.push("I don't understand what you mean by that...\n");
        reply = reply.concat(printHelp(guildname, prefix, name, namePlural));
        if (isOwner) {
          sendOwnerManual(message.author, guildname, prefix);
        }
        deleteMsg(message);
        return;
    }
    if (msg.length > 0) {
      chunk(msg, 1900, c => message.channel.send(c).catch(logger.error));
    }
    if (reply.length > 0) {
      chunk(reply, 1900, c => message.author.send(c).catch(logger.error));
    }
    if (reply.length > 0 || msg.length > 0) {
      deleteMsg(message);
    }
  });

  client.login(props.token);

  http
    .createServer(function(request, response) {
      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.end(JSON.stringify(db), 'utf-8');
    })
    .listen(props.port);

  logger.info('Server running at http://127.0.0.1:' + props.port + '/');
  logger.info('https://discordapp.com/oauth2/authorize?client_id=' + props.clientId + '&scope=bot&permissions=' + props.botPermissions + '\n');
};
