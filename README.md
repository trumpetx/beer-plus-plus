[![Discord Bots](https://discordbots.org/api/widget/status/405811389748346881.svg)](https://discordbots.org/bot/405811389748346881)

**Give Beer.  Collect Beer.  Take Beer.**

**Commands:**
*  beer++ @SomeoneSpecial @SomeoneElse
*  beer--
*  beer-- @SomeoneLessSpecial
*  beer%%
*  beer##
