const winston = require('winston');
require('winston-daily-rotate-file');
const { combine, timestamp, colorize, printf } = winston.format;
const os = require('os');

const log_format = combine(timestamp(), printf(info => `${info.timestamp} ${info.level}: ${info.message}`));

const logger = winston.createLogger({
  level: 'info',
  format: log_format,
  transports: [
    new winston.transports.DailyRotateFile({
      filename: os.homedir() + '/bpp-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d'
    })
  ]
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({ format: combine(colorize(), log_format) }));
}

module.exports = logger;
